<!DOCTYPE html>
<?php
require_once("conn.php");

session_start();

if (!isset($_SESSION['user'])) {
    header("Location: login.php");
}

$result = mysqli_query(
    $mysqli,
    "SELECT a.id, b.program_name, b.description, c.package_name, c.benefit, c.price FROM orders a
    JOIN program b ON a.program_id = b.id
    JOIN package c ON a.package_id = c.id;"
);
?>

<html lang="en">

<head>
    <title>Detail Orders</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="kuis.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>

<body>
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#about"><img src="logo.png" alt="" style="width: 90px; height: 90px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#home">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#programs">Our Programs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Why TechStart</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#pricing">Pricing</a>
                </li>
            </ul>
        </div>
        <a href="logout.php" class="login">Logout</a>
    </nav>

    <div class="container-lg-12">
        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                        <tr class="text-center">
                            <th scope="col">Program Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Package</th>
                            <th scope="col">Benefit</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($user_data = mysqli_fetch_array($result)) {
                            echo "<tr>";
                            echo "<td>" . $user_data['program_name'] . "</td>";
                            echo "<td>" . $user_data['description'] . "</td>";
                            echo "<td>" . $user_data['package_name'] . "</td>";
                            echo "<td>" . $user_data['benefit'] . "</td>";
                            echo "<td>$" . $user_data['price'] . "</td>";
                            echo "<td><a href='delete.php?id=$user_data[id]'>Delete</a></td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <footer>
        <p>
            <center>TechStart by Gia Anisa</center>
        </p>
    </footer>



</body>

</html>