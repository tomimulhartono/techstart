<!DOCTYPE html>
<?php
require_once("conn.php");

session_start();

if (!isset($_SESSION['user'])) {
    header("Location: login.php");
}

$result = mysqli_query(
    $mysqli,
    "SELECT * FROM program;"
);
?>

<html lang="en">

<head>
    <title>Detail Program</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="kuis.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#about"><img src="logo.png" alt="" style="width: 90px; height: 90px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#home">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#programs">Our Programs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Why TechStart</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#pricing">Pricing</a>
                </li>
            </ul>
        </div>
        <a href="logout.php" class="login">Logout</a>
    </nav>

    <div class="container-lg-12">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-success mb-3" data-bs-toggle="modal" data-bs-target="#addProgram">Add Program</button>
                <table class="table table-striped">
                    <thead>
                        <tr class="text-center">
                            <th scope="col">Program Name</th>
                            <th scope="col">Description</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        while ($p = mysqli_fetch_array($result)) {
                        ?>
                            <tr>
                                <td><?php echo $p['program_name'] ?></td>
                                <td><?php echo $p['description'] ?></td>
                                <td>
                                    <a href="" class="btn btn-sm btn-warning" data-bs-toggle="modal" data-bs-target="#editProgram<?php echo $p['id'] ?>">Edit</a>
                                    <a href="" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#deleteProgram<?php echo $p['id'] ?>">Delete</a>
                                </td>
                            </tr>

                        <?php
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <footer>
        <p>
            <center>TechStart by Gia Anisa</center>
        </p>
    </footer>


    <?php
    while ($p = mysqli_fetch_array($result)) {
    ?>
        <div class="modal fade" id="editProgram<?php echo $p['id'] ?>" tabindex="-1" aria-labelledby="editProgramLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editProgramLabel">Edit Program</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="edit_program.php" style="width: 100%;">
                            <input hidden type="text" class="form-control" id="program_id" name="program_id" value="<?php echo $p['id'] ?>" required>
                            <input hidden type="text" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION['user'] ?>" required>
                            <div class="mb-3">
                                <label for="program_name" class="col-form-label fw-bold">Program Name</label>
                                <input type="text" class="form-control" id="program_name" name="program_name" value="<?php echo $p['program_name'] ?>">
                            </div>
                            <div class="mb-3">
                                <label for="description" class="col-form-label fw-bold">Description</label>
                                <textarea class="form-control" name="" id=""><?php echo $p['description'] ?></textarea>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="Submit" value="Order Now" class="btn btn-primary">
                    </div>
                    </form>
                </div>
            </div>
        </div>
    <?php
    }
    ?>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>

</html>