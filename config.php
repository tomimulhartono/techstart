<?php
    session_start();
    $dbhost = "localhost";
    $dbuser = "root";
    $dbname = "techstart";
    $dbpass = "";

    $kon = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

    if (!$kon) {
        echo "<script>
                    alert('Failed Connect into Database')
                    </script>";

        die("Connection Failed" . mysqli_connect_error());
    }

    function register($request)
    {
        global $kon;

        $nama = $request['nama'];
        $email = $request['email'];
        $password = mysqli_real_escape_string($kon, $request['password']);
        $passconfirm = mysqli_real_escape_string($kon, $request['passconfirm']);

        $emailcheck = "SELECT email FROM user WHERE email = '$email'";
        $select = mysqli_query($kon, $emailcheck);

        if (!mysqli_fetch_assoc($select)) {
            if ($password == $passconfirm) {
                $password = password_hash($password, PASSWORD_DEFAULT);

                $insert = mysqli_query($kon, "INSERT INTO user VALUES('','$nama','Starter','$password','$email','User')");

                $_SESSION['registered'] = 'Berhasil registrasi, silahkan login';
                header("Location: loginadmin.php");
                exit();
            }
        }

        $_SESSION['message'] = 'Email anda sudah pernah terdaftar!';
        header("Location: register.php");
        exit();
    }

    function login($request)
    {
        global $kon;

        $email = $request['email'];
        $password = $request['password'];

        $emailcheck = "SELECT * from user Where email = '$email'";
        $select = mysqli_query($kon, $emailcheck);

        if (mysqli_num_rows($select) == 1) {

            $result = mysqli_fetch_assoc($select);

            if (password_verify($password, $result['password'])) {
                $_SESSION['id'] = $result['id'];
                $_SESSION['nama'] = $result['nama'];
                $_SESSION['email'] = $result['email'];
                $_SESSION['paket'] = $result['paket'];
                $_SESSION['login'] = 'Berhasil Login';
                header("Location: Modul.php");
                exit();
            } else {
                $_SESSION['message'] = 'Password Salah';
                header("Location: loginadmin.php");
                exit();
            }
        }
        $_SESSION['message'] = 'Gagal Login';
        header("Location: loginadmin.php");
        exit();
    }

    function ubah($request)
    {
        global $kon;

        $nama = $request['nama'];
        $email = $request['email'];
        $no_hp = $request['no_hp'];
        $color = $request['color'];
        $password = mysqli_real_escape_string($kon, $request['password']);
        $confirm = mysqli_real_escape_string($kon, $request['passconfirm']);

        $emailcheck = "SELECT email FROM user WHERE email = '$email'";
        $select = mysqli_query($kon, $emailcheck);

        if (mysqli_fetch_assoc($select)) {
            if ($password == $confirm) {
                $password = password_hash($password, PASSWORD_DEFAULT);

                $query = mysqli_query($kon, "UPDATE user SET nama ='$nama', no_hp='$no_hp', password= '$password' WHERE  email = '$email'");
                $_SESSION['nama'] = $nama;
                setcookie('color', $color, strtotime('1 days'));
                $_SESSION['changed'] = 'Berhasil update profile';

                header("Location: profile.php");
                exit();
            } else {
                $_SESSION['message'] = 'Konfirmasi Password Tidak Sesuai';
                header("Location: profile.php");
                exit();
            }
        }

        $_SESSION['message'] = 'Gagal update profile!';
        header("Location: profile.php");
        exit();
    }
