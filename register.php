<?php

require_once("conn.php");

session_start();

if (isset($_SESSION['user'])) {
  header("Location: kuis.php");
}

if (isset($_POST['register'])) {

  // filter data yang diinputkan
  $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
  $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
  // enkripsi password
  $password = password_hash($_POST["password"], PASSWORD_DEFAULT);
  $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);


  // menyiapkan query
  $sql = "INSERT INTO user (full_name, username, email, password) 
            VALUES (:name, :username, :email, :password)";
  $stmt = $db->prepare($sql);

  // bind parameter ke query
  $params = array(
    ":name" => $name,
    ":username" => $username,
    ":password" => $password,
    ":email" => $email
  );

  // eksekusi query untuk menyimpan ke database
  $saved = $stmt->execute($params);

  // jika query simpan berhasil, maka user sudah terdaftar
  // maka alihkan ke halaman login
  if ($saved) header("Location: login.php");
}

?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Registration Form</title>
  <link rel="stylesheet" href="register.css">
  <style>
    .container {
      margin-bottom: 20px;
      margin-top: 20px;
    }
  </style>
</head>

<body>
  <section>
    <div class="container">
      <h1 style="text-align:center;">TechStart Registration Form</h1>
      <form action="" method="POST">
        <label><b>Full Name</b></label><br>
        <input type="text" id="nama" name="name" required><br>
        <label><b>Email</b></label><br>
        <input type="email" id="email" name="email" required><br>
        <label><b>Username</b></label><br>
        <input type="text" id="email" name="username" required><br>
        <label><b>Password</b></label><br>
        <input type="password" id="password" name="password" required><br>
        <button name="register" type="submit">Submit</button>
      </form>
      <p>Sudah punya akun? <a href="login.php">Login</a></p>
    </div>
  </section>

</body>

</html>