<?php
include 'config.php';
if (!isset($_SESSION['user'])) {
    header("Location: login.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="kuis.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#about"><img src="logo.png" alt="" style="width: 90px; height: 90px"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#home">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#programs">Our Programs</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Why TechStart</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#pricing">Pricing</a>
                </li>
            </ul>
        </div>
        <!-- <a href="register.php" class="login">Register</a> -->
        <a href="logout.php" class="login">Logout</a>
    </nav>

    <section id="home">
        <div class="wrapper">
            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row">
                            <div class="col" style="padding-top: 100px;">
                                <h1>Build Your Startup With <span>TechStart</span></h1><br>
                                <p>Our career support and apprenticeship programs will open up new career paths for you in the digital industry, no matter your background.</p>
                                <br>
                                <a href="#programs">Explore our programs</a>
                            </div>
                            <div class="col">
                                <img src="Startup.png" class="d-block w-100" alt="...">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" id="car2">
                        <div class="row">
                            <div class="col" style="padding-top: 100px;">
                                <h1>Learn with our best <span>Mentor</span></h1><br>
                                <p>The #1 startup mentorship platform for growth-addicted founders and marketers. Learn directly from experienced industry practitioners to fast-track your digital career with TechStart.</p>
                                </p>

                                <br>
                                <a href="">Explore our programs</a>
                            </div>
                            <div class="col">
                                <img src="test.png" class="d-block w-100" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="programs">
        <div class="wrapper">
            <div class="title">
                <h1>Our Programs</h1>
            </div>
            <div class="row">
                <?php
                $data = mysqli_query($kon, "SELECT * FROM program");

                while ($p = mysqli_fetch_array($data)) {
                ?>
                    <div class="card shadow" style="width: 20rem;">
                        <img src="https://www.edureka.co/blog/wp-content/uploads/2018/12/Data-Analytics-What-is-Data-Analytics-Edureka-1.png" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h4 class="card-title"><?php echo $p['program_name'] ?></h4>
                            <p class="card-text"><?php echo $p['description'] ?></p>
                            <a href="" data-bs-toggle="modal" data-bs-target="#modalOrder<?php echo $p['id'] ?>" class="btn btn-sm">Order</a>
                        </div>
                    </div>
                <?php
                }
                ?>
            </div>
    </section>

    <section id="contact">
        <div class="wrapper">
            <div class="judul">
                <h1>Why Should TechStart?</h1>
            </div>
            <div class="container">
                <div class="title-text">
                    <strong>Program Advantages</strong>
                </div>
                <h6>Online Video Courses</h6>
                <div class="progress progress0" style="background: none; ">
                    <div class="progress-bar">
                        <div class="progressing" id="classes"></div>
                        <span class="margin"></span>
                    </div>
                    <p class="percent-num">
                    </p>
                </div>
                <h6>Flexible</h6>
                <div class="progress progress0" style="background: none; ">
                    <div class="progress-bar">
                        <div class="progressing"></div>
                        <span class="margin"></span>
                    </div>
                    <p class="percent-num">
                    </p>
                </div>
                <h6>Guarantee</h6>
                <div class="progress progress0" style="background: none; ">
                    <div class="progress-bar">
                        <div class="progressing" id="guarantee"></div>
                        <span class="margin"></span>
                    </div>
                    <p class="percent-num">
                    </p>
                </div>
            </div>

            <br />
            <br><br>



            <section id="pricing">
                <h2 class="text-center">Pricing</h2>
                <table class="table_p">
                    <tr class="text-center" id="header">
                        <th>Package</th>
                        <th>Benefit</th>
                        <th>Price</th>
                    </tr>
                    <?php
                    $data = mysqli_query($kon, "SELECT * FROM package");

                    while ($p = mysqli_fetch_array($data)) {
                    ?>
                        <tr>
                            <td><?php echo $p['package_name'] ?></td>
                            <td><?php echo $p['benefit'] ?></td>
                            <td>
                                <?php
                                if ($p['price'] > 0) {
                                    echo "$" . $p['price'];
                                } else {
                                    echo "Free";
                                }
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
            </section>

            <section id="about">
                <div class="about_us">
                    <div class="title">
                        <h1 class="text-center">About TechStart</h1>
                        <br>
                        <div class="jumbotron text-center">
                            <iframe src="https://www.youtube.com/embed/20xzrZEE068" width="400" height="250" style="border:0;"></iframe>
                            <p> <span style='font-size:25px;'>&#128205;</span>Or find us on</p>
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d64864763.58405884!2d74.47220379999996!3d-7.1224074999999925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e708920223e93ed%3A0x881196082fac3535!2sTechstart%20Digitals!5e0!3m2!1sen!2sid!4v1634775698476!5m2!1sen!2sid" width="400" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
            </section>
            <section>
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Contact Us
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Contact through email</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                giaanisaa@gmail.com
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


            </section>

            <footer>
                <p>
                    <center>TechStart by Gia Anisa</center>
                </p>
            </footer>

            <?php
            $data = mysqli_query($kon, "SELECT * FROM program");

            while ($p = mysqli_fetch_array($data)) {
            ?>
                <div class="modal fade" id="modalOrder<?php echo $p['id'] ?>" tabindex="-1" aria-labelledby="modalEditDataLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalEditDataLabel">Order Program</h5>
                                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form method="POST" action="create_order.php" style="width: 100%;">
                                    <input hidden type="text" class="form-control" id="program_id" name="program_id" value="<?php echo $p['id'] ?>" required>
                                    <input hidden type="text" class="form-control" id="user_id" name="user_id" value="<?php echo $_SESSION['user'] ?>" required>
                                    <div class="mb-3">
                                        <label for="program_name" class="col-form-label fw-bold">Program Name</label>
                                        <input type="text" class="form-control" id="program_name" name="program_name" value="<?php echo $p['program_name'] ?>" readonly>
                                    </div>
                                    <div class="mb-3">
                                        <label for="package_id" class="col-form-label fw-bold">Package</label>
                                        <select class="form-select" name="package_id" id="package_id" required>
                                            <option selected disabled>--- Select ---</option>
                                            <?php
                                            $package = mysqli_query($kon, "SELECT * FROM package");

                                            while ($pk = mysqli_fetch_array($package)) {
                                            ?>
                                                <option value="<?php echo $pk['id'] ?>">
                                                    <?php echo $pk['package_name'] ?>
                                                    <?php
                                                    if ($pk['price'] > 0) {
                                                        echo "($" . $pk['price'] . ")";
                                                    } else {
                                                        echo "(Free)";
                                                    }
                                                    ?>
                                                </option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" name="Submit" value="Order Now" class="btn btn-primary">
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>

            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

</body>

</html>