<?php
session_start();

if (isset($_SESSION['user'])) {
  header("Location: kuis.php");
}

require_once("conn.php");

$rememberme = "";

if (isset($_POST['login'])) {

  $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
  $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
  $rememberme = $_POST['rememberme'];

  $sql = "SELECT * FROM user WHERE username=:username OR email=:email";
  $stmt = $db->prepare($sql);

  // bind parameter ke query
  $params = array(
    ":username" => $username,
    ":email" => $username
  );

  $stmt->execute($params);

  $user = $stmt->fetch(PDO::FETCH_ASSOC);

  // jika user terdaftar
  if ($user) {
    // verifikasi password
    if (password_verify($password, $user["password"])) {
      // buat Session
      session_start();
      $_SESSION["user"] = $user;

      if ($rememberme == 1) {
        $cookie_name = "cookie_username";
        $cookie_value = $username;
        $cookie_time = time() + (60 * 60);
        setcookie($cookie_name, $cookie_value, $cookie_time, "/");

        $cookie_name = "cookie_password";
        $cookie_value = $password;
        $cookie_time = time() + (60 * 60);
        setcookie($cookie_name, $cookie_value, $cookie_time, "/");
      }
      // login sukses, alihkan ke halaman awal
      header("Location: kuis.php");
    }
  }
}
?>

<!DOCTYPE HTML>
<html>

<head>
  <title>Login</title>
  <link rel="stylesheet" href="loginadmin.css">

  <style>
    .container {
      margin-top: 100px
    }
  </style>
</head>

<body>

  <div class="container">
    <h2 style="text-align:center; ">Login</h2>
    <form action="" method="POST">
      <label>Username</label><br>
      <input type="text" id="email" name="username"><br>
      <label>Password</label>
      <input type="password" id="password" name="password"><br>
      <label>Remember Me</label><br>
      <input type="checkbox" name="rememberme" value="1">
      <?php
      if ($rememberme == '1') echo "checked"
      ?>
      <button name="login" type="submit">Submit</button>
    </form>
    <p>Belum punya akun? <a href="register.php">Register</a></p>
  </div>
</body>

</html>